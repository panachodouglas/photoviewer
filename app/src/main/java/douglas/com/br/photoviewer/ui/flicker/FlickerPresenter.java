package douglas.com.br.photoviewer.ui.flicker;

import android.app.Activity;

import douglas.com.br.photoviewer.data.remote.FlickerDataManager;
import douglas.com.br.photoviewer.data.remote.ResponseHandler;
import douglas.com.br.photoviewer.data.remote.response.FavoritePhotosResponse;
import douglas.com.br.photoviewer.data.remote.response.PeopleInfoResponse;
import douglas.com.br.photoviewer.helpers.Constants;
import douglas.com.br.photoviewer.ui.base.BasePresenter;

/**
 * Created by douglaspanacho on 22/01/17.
 */

public class FlickerPresenter extends BasePresenter<FlickerMvpView> {

    private FlickerDataManager mProvider;
    private Activity mActivity;
    private FlickerMvpView mMvpView;

    @Override
    public void attachView(FlickerMvpView mvpView) {
        super.attachView(mvpView);
        this.mMvpView = mvpView;
    }

    public FlickerPresenter(Activity mActivity) {
        this.mActivity = mActivity;
        mProvider = new FlickerDataManager(mActivity);
    }

    //get the favorite photos from the user id from flicker
    public void getFavoritePhotos() {
        mMvpView.showProgress();
        mProvider.getFavoritePhotos(Constants.FLICKER_FAVORITES_END_POINT_METHOD, Constants.API_KEY_FLICKER, Constants.USER_ID_FLICKER, Constants.FLICKER_FORMAT_RESPONSE, 1, new ResponseHandler.OnResponse<FavoritePhotosResponse>() {
            @Override
            public void onResponseSuccess(FavoritePhotosResponse response) {
                mMvpView.setImagesFromFlicker(response);
                mMvpView.dismissProgress();
                mMvpView.dismissSwipe();
            }

            @Override
            public void onFailure(String error, int code) {
                error.toString();
                mMvpView.dismissProgress();
            }
        });
    }


}
