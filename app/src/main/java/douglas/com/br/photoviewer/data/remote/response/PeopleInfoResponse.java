package douglas.com.br.photoviewer.data.remote.response;

/**
 * Created by douglaspanacho on 22/01/17.
 */

public class PeopleInfoResponse {
    private Person person;
    private String stat;

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public String getStat() {
        return stat;
    }

    public void setStat(String stat) {
        this.stat = stat;
    }

    public class Count {

        private Integer content;

        public Integer getContent() {
            return content;
        }

        public void setContent(Integer content) {
            this.content = content;
        }

    }

    public class Description {

        private String content;

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

    }


    public class Firstdate {

        private String content;

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

    }


    public class Firstdatetaken {

        private String content;

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

    }


    public class Location {

        private String content;

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

    }


    public class Mobileurl {

        private String content;

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

    }


    public class Person {

        private String id;
        private String nsid;
        private Integer ispro;
        private Integer canBuyPro;
        private String iconserver;
        private Integer iconfarm;
        private String pathAlias;
        private String hasStats;
        private String expire;
        private Username username;
        private Realname realname;
        private Location location;
        private Timezone timezone;
        private Description description;
        private Photosurl photosurl;
        private Profileurl profileurl;
        private Mobileurl mobileurl;
        private Photos photos;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getNsid() {
            return nsid;
        }

        public void setNsid(String nsid) {
            this.nsid = nsid;
        }

        public Integer getIspro() {
            return ispro;
        }

        public void setIspro(Integer ispro) {
            this.ispro = ispro;
        }

        public Integer getCanBuyPro() {
            return canBuyPro;
        }

        public void setCanBuyPro(Integer canBuyPro) {
            this.canBuyPro = canBuyPro;
        }

        public String getIconserver() {
            return iconserver;
        }

        public void setIconserver(String iconserver) {
            this.iconserver = iconserver;
        }

        public Integer getIconfarm() {
            return iconfarm;
        }

        public void setIconfarm(Integer iconfarm) {
            this.iconfarm = iconfarm;
        }

        public String getPathAlias() {
            return pathAlias;
        }

        public void setPathAlias(String pathAlias) {
            this.pathAlias = pathAlias;
        }

        public String getHasStats() {
            return hasStats;
        }

        public void setHasStats(String hasStats) {
            this.hasStats = hasStats;
        }

        public String getExpire() {
            return expire;
        }

        public void setExpire(String expire) {
            this.expire = expire;
        }

        public Username getUsername() {
            return username;
        }

        public void setUsername(Username username) {
            this.username = username;
        }

        public Realname getRealname() {
            return realname;
        }

        public void setRealname(Realname realname) {
            this.realname = realname;
        }

        public Location getLocation() {
            return location;
        }

        public void setLocation(Location location) {
            this.location = location;
        }

        public Timezone getTimezone() {
            return timezone;
        }

        public void setTimezone(Timezone timezone) {
            this.timezone = timezone;
        }

        public Description getDescription() {
            return description;
        }

        public void setDescription(Description description) {
            this.description = description;
        }

        public Photosurl getPhotosurl() {
            return photosurl;
        }

        public void setPhotosurl(Photosurl photosurl) {
            this.photosurl = photosurl;
        }

        public Profileurl getProfileurl() {
            return profileurl;
        }

        public void setProfileurl(Profileurl profileurl) {
            this.profileurl = profileurl;
        }

        public Mobileurl getMobileurl() {
            return mobileurl;
        }

        public void setMobileurl(Mobileurl mobileurl) {
            this.mobileurl = mobileurl;
        }

        public Photos getPhotos() {
            return photos;
        }

        public void setPhotos(Photos photos) {
            this.photos = photos;
        }

    }


    public class Photos {

        private Firstdatetaken firstdatetaken;
        private Firstdate firstdate;
        private Count count;

        public Firstdatetaken getFirstdatetaken() {
            return firstdatetaken;
        }

        public void setFirstdatetaken(Firstdatetaken firstdatetaken) {
            this.firstdatetaken = firstdatetaken;
        }

        public Firstdate getFirstdate() {
            return firstdate;
        }

        public void setFirstdate(Firstdate firstdate) {
            this.firstdate = firstdate;
        }

        public Count getCount() {
            return count;
        }

        public void setCount(Count count) {
            this.count = count;
        }

    }


    public class Photosurl {

        private String content;

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

    }


    public class Profileurl {

        private String content;

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

    }


    public class Realname {

        private String _content;

        public String getContent() {
            return _content;
        }

        public void setContent(String content) {
            this._content = content;
        }

    }


    public class Timezone {

        private String label;
        private String offset;
        private String timezoneId;

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }

        public String getOffset() {
            return offset;
        }

        public void setOffset(String offset) {
            this.offset = offset;
        }

        public String getTimezoneId() {
            return timezoneId;
        }

        public void setTimezoneId(String timezoneId) {
            this.timezoneId = timezoneId;
        }

    }

    public class Username {

        private String _content;

        public String getContent() {
            return _content;
        }

        public void setContent(String content) {
            this._content = content;
        }

    }
}