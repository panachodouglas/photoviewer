package douglas.com.br.photoviewer.ui.main;

import android.app.Activity;

import douglas.com.br.photoviewer.data.remote.FlickerDataManager;
import douglas.com.br.photoviewer.data.remote.ResponseHandler;
import douglas.com.br.photoviewer.data.remote.response.PeopleInfoResponse;
import douglas.com.br.photoviewer.helpers.Constants;
import douglas.com.br.photoviewer.ui.base.BasePresenter;
import douglas.com.br.photoviewer.ui.flicker.FlickerPresenter;

/**
 * Created by douglaspanacho on 19/01/17.
 */

public class MainPresenter extends BasePresenter<MainMvpView> {

    private FlickerDataManager mProvider;
    private Activity mActivity;
    private MainMvpView mMvpView;

    public MainPresenter(Activity mActivity) {
        this.mActivity = mActivity;
        mProvider = new FlickerDataManager(mActivity);
    }

    @Override
    public void attachView(MainMvpView mvpView) {
        super.attachView(mvpView);
        this.mMvpView = mvpView;
    }

    //get the user info, such as image and name to set in the main activity
    public void getUserInfo() {
        mMvpView.showProgress();
        mProvider.getUserInfo(Constants.FLICKER_USER_INFO_END_POINT_METHOD, Constants.API_KEY_FLICKER, Constants.USER_ID_FLICKER, Constants.FLICKER_FORMAT_RESPONSE, 1, new ResponseHandler.OnResponse<PeopleInfoResponse>() {
            @Override
            public void onResponseSuccess(PeopleInfoResponse response) {
                if (response.getPerson().getRealname() != null) {
                    mMvpView.setUserName(response.getPerson().getRealname().getContent());
                }

                mMvpView.setUserImage(generateImage(response));
                mMvpView.dismissProgress();
            }

            @Override
            public void onFailure(String error, int code) {
                mMvpView.dismissProgress();
            }

        });
    }

    //generate the link of the image with the response informations
    public String generateImage(PeopleInfoResponse response) {
        String imageLink = "https://farm" + response.getPerson().getIconfarm() + ".staticflickr.com/" +
                response.getPerson().getIconserver() + "/buddyicons/" + response.getPerson().getNsid() + ".jpg";
        return imageLink;
    }
}
