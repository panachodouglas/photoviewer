package douglas.com.br.photoviewer.helpers;

/**
 * Created by douglaspanacho on 22/01/17.
 */

public class Constants {

    // FLICKER
    public static String API_KEY_FLICKER = "9062e0c3f710b0bd28598b7a34d9acdf";
    public static String USER_ID_FLICKER = "147013721@N02";
    public static String FLICKER_FAVORITES_END_POINT_METHOD = "flickr.favorites.getList";
    public static String FLICKER_USER_INFO_END_POINT_METHOD = "flickr.people.getInfo";
    public static String FLICKER_FORMAT_RESPONSE = "json";
    public static String FLICKER_URL = "https://api.flickr.com/services/";

    //IMAGE URL FLICKER
    public static String IMAGE_URL_INTENT = "image_url";
    public static String IMAGE_TITLE_INTENT = "image_title";
    public static String USER_OWNER_ID_INTENT = "user_id";

}
