package douglas.com.br.photoviewer.ui.base;



public class BasePresenter<T extends MvpView> implements Presenter<T> {
    private T mvpView;

    @Override
    public void attachView(T mvpView) {
        this.mvpView = mvpView;
    }

    @Override
    public void detachView() {

    }
}
