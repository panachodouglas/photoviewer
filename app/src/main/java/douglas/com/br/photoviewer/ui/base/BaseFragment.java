package douglas.com.br.photoviewer.ui.base;

import android.app.ProgressDialog;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.widget.Toast;

import douglas.com.br.photoviewer.R;
import douglas.com.br.photoviewer.helpers.BaseDialogHelper;


/**
 * Created by mobile2you on 11/08/16.
 */
public class BaseFragment extends Fragment {
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private BaseDialogHelper mDialogHelper;
    private ProgressDialog mProgressDialog;


    //TOAST METHODS
    public void showToast(String string) {
        Toast.makeText(getActivity(), string, Toast.LENGTH_SHORT).show();
    }

    //PROGRESS DIALOG METHODS
    public void showProgressdialog() {
        if (!isSwipeRefeshing()) {
            if (mProgressDialog == null) {
                mProgressDialog = new ProgressDialog(getContext());
            }
            if (mProgressDialog != null)
                mProgressDialog.show();
        }
    }

    public void dismissProgressDialog() {
        if (mProgressDialog != null)
            mProgressDialog.dismiss();
    }

    //  SWIPE REFRESH METHODS
    protected void setSwipeRefresh(View view, SwipeRefreshLayout.OnRefreshListener listener) {
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh);
        if (hasSwipeRefresh()) {
            mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
            mSwipeRefreshLayout.setOnRefreshListener(listener);
        }
    }

    protected void dismissSwipeRefresh() {
        if (hasSwipeRefresh()) {
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    protected void disableSwipeRefresh() {
        if (hasSwipeRefresh()) {
            mSwipeRefreshLayout.setEnabled(false);
        }
    }

    protected boolean isSwipeRefeshing() {
        return hasSwipeRefresh() && mSwipeRefreshLayout.isRefreshing();
    }

    private boolean hasSwipeRefresh() {
        return mSwipeRefreshLayout != null;
    }

    //KEYBOARD METHODS
    public void hideSoftKeyboard() {
        if (getActivity().getCurrentFocus() != null) {
            //   ViewUtil.hideKeyboard(getActivity());
        }
    }

    public void onBackPressed() {

    }

}
