package douglas.com.br.photoviewer.ui.main;

import douglas.com.br.photoviewer.ui.base.MvpView;

/**
 * Created by douglaspanacho on 19/01/17.
 */

public interface MainMvpView extends MvpView {

    void setUserName(String name);

    void setUserImage(String image);
}
