package douglas.com.br.photoviewer.ui.main;

import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;
import douglas.com.br.photoviewer.R;
import douglas.com.br.photoviewer.ui.base.BaseActivity;
import douglas.com.br.photoviewer.ui.flicker.FlickerFragment;
import douglas.com.br.photoviewer.ui.main.adapter.MainAdapter;

public class MainActivity extends BaseActivity implements MainMvpView {

    @BindView(R.id.main_view_pager)
    ViewPager mFragmentsViewPager;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.collapsing_view)
    CollapsingToolbarLayout mCollapsingView;

    @BindView(R.id.main_appbar)
    AppBarLayout mAppBarLayout;

    @BindView(R.id.main_user_imageview)
    ImageView mUsermageView;

    @BindView(R.id.main_user_name)
    TextView mUserName;


    TextView mUserToolbarNameText;
    ImageView mUserToolbarImage;

    private MainAdapter mMainAdapter;
    private MainPresenter mPresenter;
    private View mToolbarView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setAppbarLayoutAnimation();
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        mPresenter = new MainPresenter(this);
        mPresenter.attachView(this);
        mPresenter.getUserInfo();
        setViewPagerAdapter();
        setToolbarView();

    }

    //set the toolbar view with the custom view
    public void setToolbarView() {
        mToolbarView = LayoutInflater.from(getApplicationContext()).inflate(R.layout.toolbar_view, null);
        mToolbar.addView(mToolbarView);
        mUserToolbarImage = (ImageView) mToolbarView.findViewById(R.id.main_toolbar_user_imageview);
        mUserToolbarNameText = (TextView) mToolbarView.findViewById(R.id.main_toolbar_user_name);
    }


    //used to create the animation of the toolbar, showing and hidding the toolbar
    public void setAppbarLayoutAnimation() {
        mAppBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                int maxScroll = appBarLayout.getTotalScrollRange();
                float percentage = (float) Math.abs(verticalOffset / 2) / (float) (mAppBarLayout.getHeight() - maxScroll);
                mToolbar.setAlpha(percentage);
            }
        });
    }

    //set the viewpager with the fragment
    public void setViewPagerAdapter() {
        mMainAdapter = new MainAdapter(getSupportFragmentManager());
        mMainAdapter.addFragment(new FlickerFragment());
        mFragmentsViewPager.setAdapter(mMainAdapter);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);

        final MenuItem myActionMenuItem = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) myActionMenuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (!searchView.isIconified()) {
                    searchView.setIconified(true);
                }
                myActionMenuItem.collapseActionView();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });
        return true;
    }


    //Model methods used to update the view
    @Override
    public void setUserName(String name) {
        mUserName.setText(name);
        mUserToolbarNameText.setText(name);
    }

    //set both toolbar and appbarlayout image
    @Override
    public void setUserImage(String image) {
        Glide.with(this).load(image).into(mUsermageView);
        Glide.with(this).load(image).into(mUserToolbarImage);
    }

    @Override
    public void showProgress() {
        showProgressdialog();
    }

    @Override
    public void dismissProgress() {
        dismissProgressDialog();
    }
}
