package douglas.com.br.photoviewer.ui.image_details;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import douglas.com.br.photoviewer.R;
import douglas.com.br.photoviewer.helpers.Constants;
import douglas.com.br.photoviewer.ui.base.BaseActivity;

/**
 * Created by douglaspanacho on 22/01/17.
 */

public class ImageDetailsActivity extends BaseActivity implements ImageDetailsMvpView {

    @BindView(R.id.details_image_imageview)
    ImageView mFlickerImage;

    @BindView(R.id.details_title_textview)
    TextView mFlickerImageTitle;

    @BindView(R.id.details_owner_image_imageview)
    ImageView mOwnerImage;

    @BindView(R.id.details_owner_name_textview)
    TextView mOwnerName;

    @BindView(R.id.details_progress_bar)
    ProgressBar mProgressBar;

    private String mImageUrl = "";
    private String mImageTitle = "";
    private String mUserOwnerName = "";
    private ImageDetailsPresenter mPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_details);
        ButterKnife.bind(this);
        mPresenter = new ImageDetailsPresenter(this);
        mPresenter.attachView(this);
        getInfo();
        setInfo();
    }

    //get the image info, like current image, owner picture, owner name and picture title
    public void getInfo() {
        if (getIntent() != null) {
            if (getIntent().getStringExtra(Constants.IMAGE_URL_INTENT) != null) {
                mImageUrl = getIntent().getStringExtra(Constants.IMAGE_URL_INTENT);
            }
            if (getIntent().getStringExtra(Constants.IMAGE_TITLE_INTENT) != null) {
                mImageTitle = getIntent().getStringExtra(Constants.IMAGE_TITLE_INTENT);
            }
            if (getIntent().getStringExtra(Constants.USER_OWNER_ID_INTENT) != null) {
                mUserOwnerName = getIntent().getStringExtra(Constants.USER_OWNER_ID_INTENT);
            }
        }
    }

    //set the current info to the components
    public void setInfo() {
        if (!mImageUrl.isEmpty()) {
            Glide.with(this).load(mImageUrl).listener(new RequestListener<String, GlideDrawable>() {
                @Override
                public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                    if (mProgressBar != null) {
                        mProgressBar.setVisibility(View.GONE);
                    }
                    return false;
                }

                @Override
                public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                    if (mProgressBar != null) {
                        mProgressBar.setVisibility(View.GONE);
                    }
                    return false;
                }
            }).into(mFlickerImage);
        }
        mFlickerImageTitle.setText(mImageTitle);
        mPresenter.getUserInfo(mUserOwnerName);
    }

    @OnClick(R.id.details_close_imageview)
    void closeActivity() {
        finish();
    }

    //methods from model
    @Override
    public void setOwnerName(String name) {
        mOwnerName.setText(name);
    }

    @Override
    public void setOwnerImage(String image) {
        Glide.with(this).load(image).into(mOwnerImage);
    }

    @Override
    public void showProgress() {
        showProgressdialog();
    }

    @Override
    public void dismissProgress() {
        dismissProgressDialog();
    }
}
