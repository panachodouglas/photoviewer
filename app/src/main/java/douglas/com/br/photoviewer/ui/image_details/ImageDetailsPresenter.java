package douglas.com.br.photoviewer.ui.image_details;

import android.app.Activity;

import douglas.com.br.photoviewer.data.remote.FlickerDataManager;
import douglas.com.br.photoviewer.data.remote.ResponseHandler;
import douglas.com.br.photoviewer.data.remote.response.FavoritePhotosResponse;
import douglas.com.br.photoviewer.data.remote.response.PeopleInfoResponse;
import douglas.com.br.photoviewer.helpers.Constants;
import douglas.com.br.photoviewer.ui.base.BasePresenter;

/**
 * Created by douglaspanacho on 22/01/17.
 */

public class ImageDetailsPresenter extends BasePresenter<ImageDetailsMvpView> {

    private ImageDetailsMvpView mMvpView;
    private Activity mActivity;
    private FlickerDataManager mProvider;

    public ImageDetailsPresenter(Activity mActivity) {
        this.mActivity = mActivity;
        mProvider = new FlickerDataManager(mActivity);
    }

    @Override
    public void attachView(ImageDetailsMvpView mvpView) {
        super.attachView(mvpView);
        this.mMvpView = mvpView;
    }


    //get the owner info to set the name and picture
    public void getUserInfo(String userId) {
        mProvider.getUserInfo(Constants.FLICKER_USER_INFO_END_POINT_METHOD, Constants.API_KEY_FLICKER, userId, Constants.FLICKER_FORMAT_RESPONSE, 1, new ResponseHandler.OnResponse<PeopleInfoResponse>() {
            @Override
            public void onResponseSuccess(PeopleInfoResponse response) {
                if (response.getPerson().getRealname() != null) {
                    mMvpView.setOwnerName(response.getPerson().getRealname().getContent());
                } else {
                    if (response.getPerson().getUsername() != null) {
                        mMvpView.setOwnerName(response.getPerson().getUsername().getContent());
                    }
                }

                mMvpView.setOwnerImage(generateImage(response));
            }

            @Override
            public void onFailure(String error, int code) {
            }

        });
    }

    //generate the link of the image with the response informations
    public String generateImage(PeopleInfoResponse response) {
        String imageLink = "https://farm" + response.getPerson().getIconfarm() + ".staticflickr.com/" +
                response.getPerson().getIconserver() + "/buddyicons/" + response.getPerson().getNsid() + ".jpg";
        return imageLink;
    }
}

