package douglas.com.br.photoviewer.data.remote.service;

import douglas.com.br.photoviewer.data.remote.response.FavoritePhotosResponse;
import douglas.com.br.photoviewer.data.remote.response.PeopleInfoResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by douglaspanacho on 04/01/17.
 */

public interface FlickerApiService {

    @GET("rest/")
    Call<FavoritePhotosResponse> getFavoritePhotos(@Query("method") String method, @Query("api_key") String apiKey,
                                                   @Query("user_id") String userId, @Query("format") String format, @Query("nojsoncallback") int nojsoncallback);


    @GET("rest/")
    Call<PeopleInfoResponse> getPeopleInfo(@Query("method") String method, @Query("api_key") String apiKey,
                                               @Query("user_id") String userId, @Query("format") String format, @Query("nojsoncallback") int nojsoncallback);
}
