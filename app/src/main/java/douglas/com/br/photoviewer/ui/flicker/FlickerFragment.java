package douglas.com.br.photoviewer.ui.flicker;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import douglas.com.br.photoviewer.R;
import douglas.com.br.photoviewer.data.remote.response.FavoritePhotosResponse;
import douglas.com.br.photoviewer.helpers.Constants;
import douglas.com.br.photoviewer.ui.base.BaseFragment;
import douglas.com.br.photoviewer.ui.flicker.adapter.FlickerImagesAdapter;
import douglas.com.br.photoviewer.ui.image_details.ImageDetailsActivity;

/**
 * Created by douglaspanacho on 21/01/17.
 */

public class FlickerFragment extends BaseFragment implements FlickerMvpView {

    @BindView(R.id.flicker_recyler_view)
    RecyclerView mRecyclerView;

    private StaggeredGridLayoutManager mStaggeredGridLayoutManager;
    private FlickerImagesAdapter mAdapter;
    private FlickerPresenter mPresenter;
    private FavoritePhotosResponse mFavoriteItemsResponse;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_flicker, container, false);
        ButterKnife.bind(this, view);
        setRecyclerView();
        mPresenter = new FlickerPresenter(getActivity());
        mPresenter.attachView(this);
        mPresenter.getFavoritePhotos();
        setSwipeRefresh(view, new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPresenter.getFavoritePhotos();
            }
        });
        return view;
    }

    //set the recycler view and it's adapter
    public void setRecyclerView() {
        mAdapter = new FlickerImagesAdapter(new FlickerImagesAdapter.OnItemCLicked() {
            @Override
            public void onItemClicked(int position, String imageUrl) {
                Intent intent = new Intent(getContext(), ImageDetailsActivity.class);
                intent.putExtra(Constants.IMAGE_URL_INTENT, imageUrl);
                if (mFavoriteItemsResponse != null) {
                    intent.putExtra(Constants.IMAGE_TITLE_INTENT, mFavoriteItemsResponse.getPhotos().getPhoto().get(position).getTitle());
                    intent.putExtra(Constants.USER_OWNER_ID_INTENT, mFavoriteItemsResponse.getPhotos().getPhoto().get(position).getOwner());
                }
                startActivity(intent);
            }
        });
        mStaggeredGridLayoutManager = new StaggeredGridLayoutManager(2, LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(mStaggeredGridLayoutManager);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setItemViewCacheSize(20);
        mRecyclerView.setDrawingCacheEnabled(true);
        mRecyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        mRecyclerView.setAdapter(mAdapter);
    }


    //update adapter with the images of flicker
    @Override
    public void setImagesFromFlicker(FavoritePhotosResponse response) {
        mAdapter.updateAdapter(response);
        mFavoriteItemsResponse = response;
    }

    @Override
    public void dismissSwipe() {
        dismissSwipeRefresh();
    }


    @Override
    public void showProgress() {
        showProgressdialog();
    }

    @Override
    public void dismissProgress() {
        dismissProgressDialog();
    }
}
