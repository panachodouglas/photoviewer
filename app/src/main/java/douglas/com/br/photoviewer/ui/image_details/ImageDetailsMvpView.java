package douglas.com.br.photoviewer.ui.image_details;

import douglas.com.br.photoviewer.ui.base.MvpView;

/**
 * Created by douglaspanacho on 22/01/17.
 */

public interface ImageDetailsMvpView extends MvpView {
    void setOwnerName(String name);

    void setOwnerImage(String image);
}
