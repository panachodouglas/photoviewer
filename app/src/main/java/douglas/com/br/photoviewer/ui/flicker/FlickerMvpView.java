package douglas.com.br.photoviewer.ui.flicker;

import douglas.com.br.photoviewer.data.remote.response.FavoritePhotosResponse;
import douglas.com.br.photoviewer.ui.base.MvpView;

/**
 * Created by douglaspanacho on 22/01/17.
 */

public interface FlickerMvpView extends MvpView {

    void setImagesFromFlicker(FavoritePhotosResponse response);

    void dismissSwipe();


}
