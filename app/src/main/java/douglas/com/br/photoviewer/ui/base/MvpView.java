package douglas.com.br.photoviewer.ui.base;


public interface MvpView {

    void showProgress();

    void dismissProgress();
}
