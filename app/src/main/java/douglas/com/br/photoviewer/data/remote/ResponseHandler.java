package douglas.com.br.photoviewer.data.remote;

import android.app.Activity;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mobile2you on 11/08/16.
 */
public class ResponseHandler<T> {

    //1xx Informational
    public static final int CODE_WITHOUT_NETWORK = 0;

    //2xx Success
    public static final int CODE_RESPONSE_SUCCESS = 200;

    //3xx Redirection
    public static final int CODE_NOT_FOUND = 340;

    //4xx Client Error
    public static final int CODE_TIMEOUT = 408;
    public static final int CODE_RESPONSE_UNAUTHORIZED = 401;
    public static final int CODE_BAD_REQUEST = 400;
    public static final int CODE_FORBIDDEN = 403;

    //5xx Server Error
    public static final int CODE_UNKNOWN = 500;

    private Activity mActivity;

    public ResponseHandler(Activity activity) {
        mActivity = activity;
    }

    public interface OnResponse<T> {
        void onResponseSuccess(T response);

        void onFailure(String error, int code);
    }

    private void handleResponse(Response<T> response, OnResponse<T> handler) {
        if (response.isSuccessful() && response.code() == CODE_RESPONSE_SUCCESS) {
            handler.onResponseSuccess(response.body());
        } else if (response.code() == CODE_UNKNOWN) {
            handler.onFailure(response.message(), response.code());
        } else {
            handler.onFailure(response.message(), response.code());
        }
    }

    public Callback<T> retrofitCallback(final OnResponse<T> handler) {
        return new Callback<T>() {
            @Override
            public void onResponse(Call<T> call, Response<T> response) {
                if (mActivity != null && !mActivity.isFinishing()) {
                    handleResponse(response, handler);
                }
            }

            @Override
            public void onFailure(Call<T> call, Throwable t) {
                if (mActivity != null && !mActivity.isFinishing()) {
                }
            }
        };
    }
}
