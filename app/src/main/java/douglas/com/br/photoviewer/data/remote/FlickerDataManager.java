package douglas.com.br.photoviewer.data.remote;

import android.app.Activity;

import java.util.ArrayList;
import java.util.List;


import douglas.com.br.photoviewer.data.remote.response.FavoritePhotosResponse;
import douglas.com.br.photoviewer.data.remote.response.PeopleInfoResponse;
import douglas.com.br.photoviewer.data.remote.service.FlickerApiService;
import douglas.com.br.photoviewer.helpers.Constants;
import okhttp3.Interceptor;

/**
 * Created by douglaspanacho on 04/01/17.
 */

public class FlickerDataManager extends BaseProvider {

    private Activity activity;
    private FlickerApiService mApi;

    public FlickerDataManager(Activity activity) {
        super(activity);
        this.activity = activity;
        List<Interceptor> interceptors = new ArrayList<>();
        mApi = getApi(interceptors);


    }

    public FlickerApiService getApi(List<Interceptor> interceptors) {
        mApi = getRetrofit(interceptors).create(FlickerApiService.class);
        return mApi;
    }

    public void getFavoritePhotos(String method, String apiKey, String userId, String responseFormat,int noCallBack , ResponseHandler.OnResponse<FavoritePhotosResponse> listener) {
        enqueueRequest(mApi.getFavoritePhotos(method, apiKey, userId, responseFormat,noCallBack), listener);
    }

    public void getUserInfo(String method, String apiKey, String userId, String responseFormat,int noCallBack , ResponseHandler.OnResponse<PeopleInfoResponse> listener) {
        enqueueRequest(mApi.getPeopleInfo(method, apiKey, userId, responseFormat,noCallBack), listener);
    }

    @Override
    public Activity getActivity() {
        return activity;
    }

    @Override
    public String getUrl() {
        return Constants.FLICKER_URL;
    }
}
