package douglas.com.br.photoviewer.data.remote;

import android.app.Activity;

import java.util.List;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by mobile2you on 19/07/16.
 */
public abstract class BaseProvider<T> {

    // protected Activity mActivity;
    private ResponseHandler<T> mResponseHandler;

    public abstract Activity getActivity();

    public BaseProvider(Activity activity) {
        mResponseHandler = new ResponseHandler<>(activity);
    }


    public abstract String getUrl();


    public Retrofit getRetrofit(List<Interceptor> interceptors) {
        Retrofit.Builder retroBuilder = new Retrofit.Builder().baseUrl(getUrl()).addConverterFactory(GsonConverterFactory.create());
        OkHttpClient.Builder httpClient = getClient(interceptors);
        retroBuilder.client(httpClient.build());
        return retroBuilder.build();
    }

    private OkHttpClient.Builder getClient(List<Interceptor> interceptors) {
        OkHttpClient.Builder httpClientBuilder = new OkHttpClient.Builder();
        return httpClientBuilder;
    }


    protected void enqueueRequest(Call<T> call, ResponseHandler.OnResponse<T> listener) {
        call.enqueue(mResponseHandler.retrofitCallback(listener));
    }
}

