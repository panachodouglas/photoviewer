package douglas.com.br.photoviewer.ui.flicker.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import douglas.com.br.photoviewer.R;
import douglas.com.br.photoviewer.data.remote.response.FavoritePhotosResponse;

/**
 * Created by douglaspanacho on 21/01/17.
 */

public class FlickerImagesAdapter extends RecyclerView.Adapter {

    private OnItemCLicked mListener;
    private FavoritePhotosResponse mItems = new FavoritePhotosResponse();

    public FlickerImagesAdapter(OnItemCLicked mListener) {
        this.mListener = mListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_big_image_flicker, parent, false);
        return new FlickerImagesViewHolder(view);
    }

    public void updateAdapter(FavoritePhotosResponse items) {
        this.mItems = items;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((FlickerImagesViewHolder) holder).bind(position);
    }

    @Override
    public int getItemCount() {
        return mItems.getPhotos().getPhoto().size();
    }


    class FlickerImagesViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.flicker_item_imageview)
        ImageView mImageView;


        public FlickerImagesViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(final int position) {
            Glide.with(itemView.getContext()).load(generateImage(position)).into(mImageView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mListener.onItemClicked(position, generateImage(position));
                }
            });
        }

        public String generateImage(int position) {
            String imageLink = "https://farm" + mItems.getPhotos().getPhoto().get(position).getFarm() + ".staticflickr.com/" +
                    mItems.getPhotos().getPhoto().get(position).getServer() + "/" + mItems.getPhotos().getPhoto().get(position).getId() + "_" +
                    mItems.getPhotos().getPhoto().get(position).getSecret() + ".jpg";
            return imageLink;
        }
    }


    public interface OnItemCLicked {
        void onItemClicked(int position, String imageUrl);
    }
}
